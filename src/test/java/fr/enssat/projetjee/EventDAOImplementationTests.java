package fr.enssat.projetjee;

import org.junit.jupiter.api.*;

import java.sql.SQLException;
import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EventDAOImplementationTests {
    @BeforeAll
    public static void tearDown() {
        DBPopulate dbPopulate = new DBPopulate();
        dbPopulate.EventDelete();
        dbPopulate.PlaceDelete();
        dbPopulate.DJDelete();
    }

    @AfterAll
    public static void cleanUpAfterAll() {
        DBPopulate dbPopulate = new DBPopulate();
        dbPopulate.EventDelete(); // Delete event first (constraints)
        dbPopulate.PlaceDelete();
        dbPopulate.DJDelete();
        dbPopulate.PlacePopulate();
        dbPopulate.DJPopulate();
        dbPopulate.EventPopulate();
    }

    @Test
    @DisplayName("Test find by all, id and date")
    void testFindEvent() {
        // Create 2 events
        EventDAOImplementation eventDAO = new EventDAOImplementation();
        // Create a DJ and a Place
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place = placeDAO.create(new Place( "address", "city", "zip", "bloblbob"));
        DJDAOImplementation djDAO = new DJDAOImplementation();
        DJ dj = djDAO.create(new DJ("name", "surname", "2000-01-01", "scenename", "residency", Style.ELECTRO));
        String startDate = "2024-04-01 00:00:00";
        String endDate = "2024-04-02 01:00:00";
        Event event1 = new Event(startDate, endDate, dj.getId(), place.getId());
        Event eventCreated1 = eventDAO.create(event1);
        String startDate2 = "2025-05-03 00:00:00";
        String endDate2 = "2025-05-04 01:00:00";
        Event event2 = new Event(startDate2, endDate2, dj.getId(), place.getId());
        Event eventCreated2 = eventDAO.create(event2);
        // Test find by id
        assertEquals(event1, eventDAO.findById(eventCreated1.getId()));
        assertEquals(event2, eventDAO.findById(eventCreated2.getId()));
        // Test find by date
        assertEquals(1, eventDAO.findByDate("2024-04-01 00:00:00").size());
        assertEquals(1, eventDAO.findByDate("2025-05-04 01:00:00").size());
    }

    @Test
    @DisplayName("Test create, update and delete event")
    void testCreateDeleteEvent() {
        EventDAOImplementation eventDAO = new EventDAOImplementation();
        // Create a DJ and a Place
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place = placeDAO.create(new Place( "address", "city", "zip", "bloblbob"));
        DJDAOImplementation djDAO = new DJDAOImplementation();
        DJ dj = djDAO.create(new DJ("name", "surname", "2000-01-01", "scenename", "residency", Style.EDM));
        String startDate = "2021-04-01 00:00:00";
        String endDate = "2021-04-02 01:00:00";
        // Create DJ and Place (Foreign key constraints)
        Event event = new Event(startDate, endDate, dj.getId(), place.getId());
        Event eventCreated = eventDAO.create(event);
        assertEquals(event, eventCreated);
        assertEquals(event, eventDAO.findById(eventCreated.getId()));
        Event eventDeleted = eventDAO.delete(eventCreated.getId());
        assertNull(eventDAO.findById(eventCreated.getId()));
        assertEquals(event, eventDeleted);
        event.setStartDate("2021-05-01 00:00:00");
        event.setEndDate("2021-05-02 01:00:00");
    }

    @Test
    @DisplayName("Test BDD constraints")
    public void testBDDConstraints() {
        EventDAOImplementation eventDAO = new EventDAOImplementation();
        // Create a DJ and a Place
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place = placeDAO.create(new Place( "address", "city", "zip", "bloblbob"));
        DJDAOImplementation djDAO = new DJDAOImplementation();
        DJ dj = djDAO.create(new DJ("name", "surname", "2000-01-01", "scenename", "residency", Style.ELECTRO));
        // Test start date < end date
        String startDate = "2021-04-01 00:00:00";
        String endDate = "2021-04-02 01:00:00";
        Event event = new Event(startDate, endDate, dj.getId(), place.getId());
        Event eventCreated = eventDAO.create(event);
        // test simultaneous events
        String startDate2 = "2021-04-01 15:00:00";
        String endDate2 = "2021-04-02 20:06:00";
        Event event2 = new Event(startDate2, endDate2, dj.getId(), place.getId());
        assertEquals(0,eventDAO.create(event2).getId());
        // same with a different continent for the place
        place = placeDAO.create(new Place( "address", "city", "zip", "angnerugntru"));
        String startDate3 = "2021-04-03 00:21:00";
        String endDate3 = "2021-04-04 01:00:00";
        Event event3 = new Event(startDate3, endDate3, dj.getId(), place.getId());
        // 48h differences: not enough for two continents
        assertEquals(0, eventDAO.create(event3).getId());
        // 24h should pass for same continent
        place.setContinent("Europe");
        placeDAO.update(place);
        Event event4 = new Event(startDate3, endDate3, dj.getId(), place.getId());
        assertEquals(event4, eventDAO.create(event4));
    }
}
