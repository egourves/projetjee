package fr.enssat.projetjee;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DJDAOImplementationTests {
    @BeforeAll
    @AfterAll
    static void clean() {
        DBPopulate dbPopulate = new DBPopulate();
        dbPopulate.DJDelete();
        dbPopulate.DJPopulate();
    }

    @Test
    @DisplayName("Test findByAll, findById, create, update, delete")
    void testCreateDeleteDJ() {
        DJDAOImplementation djDAO = new DJDAOImplementation();
        ArrayList<DJ> djs = djDAO.findByAll();
        int size = djs.size();
        DJ dj = new DJ("DJ3", "DJ3", "2000-01-01", "DJ3", "Rennes", Style.ELECTRO);
        DJ djCreated = djDAO.create(dj);
        // Test create
        assertEquals(size + 1, djDAO.findByAll().size());
        assertEquals(dj.getName(), djCreated.getName());
        assertEquals(djCreated.getName(), djDAO.findById(djCreated.getId()).getName());
        // Test update
        dj.setSceneName("DJ3E");
        djDAO.update(dj);
        assertEquals(dj.getSceneName(), djDAO.findById(djCreated.getId()).getSceneName());
        DJ djDeleted = djDAO.delete(djCreated.getId());
        // Test delete
        assertEquals(size, djDAO.findByAll().size());
        assertEquals(dj.getId(), djDeleted.getId());
        assertEquals(dj, djDeleted);
    }

    @Test
    @DisplayName("Test findByName")
    void testFindByName() {
        DJDAOImplementation djDAO = new DJDAOImplementation();
        assertEquals("Lina", (djDAO.findByName("Lina").get(0)).getSurname());
        assertEquals("Jonsson", (djDAO.findByName("Lina").get(0)).getName());
        assertEquals("SPFDJ", (djDAO.findByName("Lina").get(0)).getSceneName());
    }
}
