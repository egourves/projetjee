package fr.enssat.projetjee;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class PlaceDAOImplementationTests {
    @BeforeAll
    @AfterAll
    static void clean() {
        DBPopulate dbPopulate = new DBPopulate();
        dbPopulate.PlaceDelete();
        dbPopulate.PlacePopulate();
    }

    @Test
    @DisplayName("Test create and delete place")
    void testCreateDeletePlace() {
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place = new Place("Club1", "Address1", "France", "Europe");
        Place placeCreated = placeDAO.create(place);
        // Check if the place is created (id updated, !=0 and in the database)
        assertEquals(place, placeDAO.findById(placeCreated.getId()));
        Place placeDeleted = placeDAO.delete(placeCreated.getId());
        // Check if the deleted place is the same as the created place
        assertEquals(place, placeDeleted);
        // Check if the place is removed from the database
        assertNull(placeDAO.findById(placeCreated.getId()));
    }

    @Test
    @DisplayName("Test update place")
    void testUpdatePlace() {
        // Create a palce to update
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place = new Place("Club2", "Address2", "France", "Europe");
        Place placeCreated = placeDAO.create(place);
        // Update the place
        place.setClubName("Club3");
        placeDAO.update(place);
        assertEquals(place, placeCreated);
        // Check if the place is updated in the database
        assertEquals(place.getClubName(), (placeDAO.findById(placeCreated.getId())).getClubName());
    }

    @Test
    @DisplayName("Test delete all places")
    void testDeleteAllPlaces() {
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place1 = new Place("Club4", "Address4", "France", "Europe");
        Place place2 = new Place("Club5", "Address5", "France", "Europe");
        Place placeCreated1 = placeDAO.create(place1);
        Place placeCreated2 = placeDAO.create(place2);
        DBPopulate dbPopulate = new DBPopulate();
        dbPopulate.PlaceDelete();
        // Check if all places are removed from the database
        assertNull(placeDAO.findById(placeCreated1.getId()));
        assertNull(placeDAO.findById(placeCreated2.getId()));
        assertTrue(placeDAO.findByAll().isEmpty());
    }

    @Test
    @DisplayName("Test find all places")
    void testFindAllPlaces() {
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place1 = new Place("Club6", "Address6", "France", "Europe");
        Place place2 = new Place("Club7", "Address7", "France", "Europe");
        placeDAO.create(place1);
        placeDAO.create(place2);
        assertEquals(2, placeDAO.findByAll().size());
    }

    @Test
    @DisplayName("Test delete place")
    void testDeletePlace() {
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place = new Place("Club8", "Address8", "France", "Europe");
        Place placeCreated = placeDAO.create(place);
        Place placeDeleted = placeDAO.delete(placeCreated.getId());
        assertNull(placeDAO.findById(placeCreated.getId()));
        assertEquals(place, placeDeleted);
    }

    @Test
    @DisplayName("Test find place by id, city, country, club name")
    void testFindPlaceById() {
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        // Weird names to avoid conflicts with the populated database
        Place place = new Place("rhtrhytjtyjtyj", "Address9", "grongjtnhtjnhrt", "Europe");
        Place place2 = new Place("rhtrhytjtyjtyj", "hrhtrhtjtjyt", "grgregrgerger", "Europe");
        Place place3 = new Place("tjrtjrjryjrhethtrhrt", "Address11", "grongjtnhtjnhrt", "Asie");
        Place placeCreated = placeDAO.create(place);
        placeDAO.create(place2);
        placeDAO.create(place3);
        assertEquals(place, placeDAO.findById(placeCreated.getId()));
        assertEquals(1, placeDAO.findByCity("hrhtrhtjtjyt").size());
        assertEquals(1, placeDAO.findByCountry("grgregrgerger").size());
        assertEquals(2, placeDAO.findByCountry("grongjtnhtjnhrt").size());
        assertEquals(2, placeDAO.findByClubName("rhtrhytjtyjtyj").size());
        assertEquals(1, placeDAO.findByClubName("tjrtjrjryjrhethtrhrt").size());
    }

    @Test
    @DisplayName("Test if place exists check")
    void testPlaceExists() {
        // Check directly if the place exists
        PlaceDAOImplementation placeDAO = new PlaceDAOImplementation();
        Place place = new Place("Club12", "Address12", "France", "Europe");
        assertFalse(placeDAO.isPlaceExist(place));
        Place placeCreated = placeDAO.create(place);
        assertTrue(placeDAO.isPlaceExist(placeCreated));
        // Check by creating two places with the same attributes
        Place place2 = new Place("Club12", "Address12", "France", "Europe");
        // If the place is inserted in the database, create should return the place with an id
        assertEquals(0, placeDAO.create(place2).getId());
    }
}
