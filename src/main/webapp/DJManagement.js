let api_url = "/projetjee_war_exploded/api/djs/";
let musicStyles = ["ELECTRO","HOUSE","HARD_STYLE","EDM"]

/**
 * S'occupe du chargement de la page DJListing
 * @param url : string : url de l'api
 */
function init_listing(url=api_url) {
    $('#DJTable').DataTable({
        language: {
            url: '//cdn.datatables.net/plug-ins/2.0.2/i18n/fr-FR.json',
        },
        columnDefs: [
            { targets: [0], visible: false }, // Cache la première colonne (id)
        ]
    }); // Initialise DataTables
    loadDJs(url);
    loadTop5DJs(url+"/top5");
}

/**
 * S'occupe du chargement de la page DJManagement
 * @param url : string : url de l'api
 */
function init_management(url) {
    let dj_id_modify;
    let table = $('#DJTable').DataTable({
        language: {
            url: '//cdn.datatables.net/plug-ins/2.0.2/i18n/fr-FR.json',
        },
        columnDefs: [
            //{ targets: [0], visible: false }, // Cache la première colonne (id)
            {data: null, defaultContent: '<button class="button-modify">Modifier</button><button class="button-delete">Supprimer</button>', targets: -1}]
    }); // Initialise DataTables

    loadDJs(url);

    // Partie pour gérer le formulaire de modification
    // on associe des actions aux boutons présents dans la table des DJ (DJManagement uniquement)
    // et on pré-remplit le formulaire

    const dialogModify = document.getElementById("dialog-modify");
    const confirmBtn = dialogModify.querySelector("#confirmBtn");
    const cancelBtn = dialogModify.querySelector("#cancelBtn");

    table.on('click', '.button-modify', function (e) {
        let dj = table.row(e.target.closest('tr')).data();
        dialogModify.querySelector("#modifySceneName").value = dj[1];
        dialogModify.querySelector("#modifyStyle").selectedIndex = musicStyles.indexOf(dj[6])+1;
        dialogModify.querySelector("#modifyName").value = dj[2];
        dialogModify.querySelector("#modifySurname").value = dj[3];
        dialogModify.querySelector("#modifyBirth").value = dj[4];
        dialogModify.querySelector("#modifyResidence").value = dj[5];
        dialogModify.showModal();
        dj_id_modify = dj[0];
    });

    // Requête DELETE sur api/djs/{id du DJ} conformément à l'api REST
    table.on('click', '.button-delete', function (e) {
        let dj = table.row(e.target.closest('tr')).data();
        if (confirm('Voulez-vous supprimer ' + dj[1] + ' ?')) {
            //console.log("supprimer id:" + dj[0]);
            fetch("/projetjee_war_exploded/api/djs/" + dj[0], {method: 'DELETE'})
                .then(() => loadDJs(api_url));
        }
    });

    cancelBtn.addEventListener("click", (event) => {
        dialogModify.close()
    });

    confirmBtn.addEventListener("click", (event) => {
        let dj = get_DJ_from_form("modify");
        //console.log("id:", dj_id_modify, " ; Nom de scène :", dj.dj_sceneName);
        send_request(api_url + dj_id_modify, 'PUT', dj)
        dialogModify.close();
    });

    // Pour intercepter le déroulement du formulaire d'ajout
    // et ajouter une fonction pour vider le formulaire
    // en plus de l'envoi de la requête POST
    let form = document.getElementById('form-create');
    if (form.attachEvent) {
        form.attachEvent("submit", processForm);
    } else {
        form.addEventListener("submit", processForm);
    }
}

/**
 * Récupère les données des DJs via l'api, avec une requête GET
 * @param url : string : url de l'api
 */
function loadDJs(url=api_url) {
    fetch(url)
        .then(response => response.json())
        .then(response => update_table(response))
        .catch(error => alert("Erreur : " + error));
}

/**
 * Met à jour la table des DJs
 * @param djs : Array : array contenant tous les DJs
 */
function update_table(djs) {
    let table = $('#DJTable').DataTable();
    table.clear(); // Efface les données existantes dans le tableau

    djs.forEach(dj => {
        table.row.add([ // Ajoute une nouvelle ligne de données au tableau
            dj.id,
            dj.sceneName,
            dj.name,
            dj.surname,
            dj.birthDate,
            dj.residence,
            dj.style,
        ]).draw(); // Redessine le tableau avec les nouvelles données
    });
}

/**
 * Récupère le top5 des DJs via l'api, avec une requête GET
 * @param url : string : url de l'api
 */
function loadTop5DJs(url) {
    fetch(url)
        .then(response => response.json())
        .then(response => update_top5(response))
        .catch(error => alert("Erreur : " + error));
}

/**
 * Met à jour le top5 des DJs
 * @param djs : Array : array contenant le top5 des DJs
 */
function update_top5(djs) {
    for (let i = 0; i < djs.length; i++) {
        console.log("djs[i].sceneName :"+djs[i].sceneName);
        $('#dj'+(i+1).toString()).text(djs[i].sceneName);
    }
}

/**
 * Récupère les données du formulaire d'ajout, envoit une requête POST puis vide le formulaire
 * @param e : Event : évènement causé par le clic sur le bouton submit
 * @return false
 */
function processForm(e) {
    if (e.preventDefault) e.preventDefault();

    let dj = get_DJ_from_form("input");
    send_request(api_url,'POST',dj)
    clear_form("input");

    return false; // prévenir le comportement habituel
}

/**
 * Récupère les données d'un formulaire
 * @param name : String : nom donné au formulaire dans les id="..."
 */
function get_DJ_from_form(name) {
    return {
        dj_sceneName : document.getElementById(name+"SceneName").value,
        dj_name : document.getElementById(name+"Name").value,
        dj_surname : document.getElementById(name+"Surname").value,
        dj_birth : document.getElementById(name+"Birth").value,
        dj_residence : document.getElementById(name+"Residence").value,
        dj_style : document.getElementById(name+"Style").value,
    };
}

/**
 * Envoi une requête "method" à l'url "url" avec les données "data"
 * @param url : String : url de l'api
 * @param method : String : GET, POST, PUT, DELETE
 * @param data : Array : les données
 */
function send_request(url,method,data) {
    // Construire une chaîne de requête avec les données au format x-www-form-urlencoded
    let urlEncodedData = '';
    for (let key in data) {
        if (urlEncodedData !== '') {
            urlEncodedData += '&';
        }
        urlEncodedData += encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }

    // Envoyer la requête avec la méthode choisie, puis actualiser la table
    fetch(url, {
        method: method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: urlEncodedData
    }).then(() => loadDJs(api_url))
}

/**
 * Vide le questionnaire (le placeholder de la balise <select> ne réapparaît pas,
 * mais la valeur ne passe pas le paramètre 'required' donc c'est suffisant fonctionnellement)
 */
function clear_form(name) {
    document.getElementById(name+"SceneName").value = "";
    document.getElementById(name+"Name").value = "";
    document.getElementById(name+"Surname").value = "";
    document.getElementById(name+"Birth").value = "";
    document.getElementById(name+"Residence").value = "";
    document.getElementById(name+"Style").selectedIndex = -1;
}

