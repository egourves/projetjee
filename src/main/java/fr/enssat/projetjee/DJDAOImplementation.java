package fr.enssat.projetjee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;

public class DJDAOImplementation implements DJDAO {
    @Override
    public ArrayList<DJ> findByAll() {
        Connection connection = null;
        Statement statement = null;
        ArrayList<DJ> djs = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.createStatement();
            var rs = statement.executeQuery("SELECT id,nom,prenom,nom_de_scene,date_naissance,lieu_residence,style_musical FROM DJ;");
            while (rs.next()) {
                djs.add(new DJ(rs.getInt("id"), rs.getString("nom"), rs.getString("prenom"), rs.getString("nom_de_scene"), rs.getString("date_naissance"), rs.getString("lieu_residence"), Style.values()[rs.getInt("style_musical")]));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return djs;
    }

    @Override
    public DJ findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        DJ dj = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, nom, prenom, nom_de_scene, date_naissance, lieu_residence, style_musical FROM DJ WHERE id = ?");
            statement.setInt(1, (id));
            var rs = statement.executeQuery();
            if (rs.next()) {
                dj = new DJ(rs.getInt("id"), rs.getString("nom"), rs.getString("prenom"), rs.getString("nom_de_scene"), rs.getString("date_naissance"), rs.getString("lieu_residence"), Style.values()[rs.getInt("style_musical")]);
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return dj;
    }

    @Override
    public ArrayList<DJ> findByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ArrayList<DJ> djs = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, nom, prenom, nom_de_scene, date_naissance, lieu_residence, style_musical FROM DJ WHERE nom = ? OR prenom = ? OR nom_de_scene = ?");
            statement.setString(1, name);
            statement.setString(2, name);
            statement.setString(3, name);
            var rs = statement.executeQuery();
            if (rs.next()) {
                djs.add(new DJ(rs.getInt("id"), rs.getString("nom"), rs.getString("prenom"), rs.getString("nom_de_scene"), rs.getString("date_naissance"), rs.getString("lieu_residence"), Style.values()[rs.getInt("style_musical")]));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return djs;
    }

    @Override
    public DJ create(DJ dj) {
        if (!isDjExist(dj)) {

            Connection connection = null;
            PreparedStatement statement = null;
            try {
                connection = DBManager.getInstance().getConnection();
                connection.setAutoCommit(false);
                statement = connection.prepareStatement("INSERT INTO DJ (nom, prenom, nom_de_scene, date_naissance, lieu_residence, style_musical) VALUES (?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, dj.getName());
                statement.setString(2, dj.getSurname());
                statement.setString(3, dj.getSceneName());
                statement.setString(4, dj.getBirthDate());
                statement.setString(5, dj.getResidence());
                statement.setInt(6, dj.getStyle().ordinal());
                statement.executeUpdate();
                connection.commit();
                var rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    dj.setId(rs.getInt(1));
                }
            } catch (Exception e) {
                System.out.println("Erreur:" + e);
            } finally {
                // Close connection and statement after execution
                if (connection != null) {
                    try {
                        if (statement != null) {
                            statement.close();
                        }
                        connection.close();
                    } catch (Exception e) {
                        System.out.println("Erreur:" + e);
                    }
                }
            }
        }
        return dj;
    }

    @Override
    public void update(DJ dj) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("UPDATE DJ SET nom = ?, prenom = ?, nom_de_scene = ?, date_naissance = ?, lieu_residence = ?, style_musical = ? WHERE id = ?");
            statement.setString(1, dj.getName());
            statement.setString(2, dj.getSurname());
            statement.setString(3, dj.getSceneName());
            statement.setString(4, dj.getBirthDate());
            statement.setString(5, dj.getResidence());
            statement.setInt(6, dj.getStyle().ordinal());
            statement.setInt(7, dj.getId());
            statement.executeUpdate();
            connection.commit();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }

    @Override
    public DJ delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        DJ dj = findById(id);
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("DELETE FROM DJ WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.commit();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return dj;
    }

    public boolean isDjExist(DJ dj) {
        Connection connection = null;
        PreparedStatement statement = null;
        boolean exist = false;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id FROM DJ WHERE nom = ? AND prenom = ? AND nom_de_scene = ? AND date_naissance = ? AND lieu_residence = ? AND style_musical = ?");
            statement.setString(1, dj.getName());
            statement.setString(2, dj.getSurname());
            statement.setString(3, dj.getSceneName());
            statement.setString(4, dj.getBirthDate());
            statement.setString(5, dj.getResidence());
            statement.setInt(6, dj.getStyle().ordinal());
            var rs = statement.executeQuery();
            if (rs.next()) {
                exist = true;
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return exist;
    }

    /**
     * @return ArrayList<DJ> : top 5 DJs
     */
    public ArrayList<DJ> top5DJs() {
        // Fixed query to get top 5 DJs
        String query = "SELECT DJ.id, nom, prenom, nom_de_scene, date_naissance, lieu_residence, style_musical, COUNT(*) as nb FROM DJ JOIN Evenement ON DJ.id = Evenement.dj GROUP BY DJ.id ORDER BY nb DESC LIMIT 5";
        Connection connection = null;
        Statement statement = null;
        ArrayList<DJ> djs = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.createStatement();
            var rs = statement.executeQuery(query);
            while (rs.next()) {
                djs.add(new DJ( rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getString("nom_de_scene"),
                        rs.getString("date_naissance"),
                        rs.getString("lieu_residence"),
                        Style.values()[rs.getInt("style_musical")]));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return djs;
    }
}
