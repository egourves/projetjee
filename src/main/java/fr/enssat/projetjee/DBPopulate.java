package fr.enssat.projetjee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

public class DBPopulate {
    public void PlacePopulate() {
        ArrayList<Place> places = new ArrayList<>();
        places.add(new Place("Berghain", "Berlin", "Allemagne", "Europe"));
        places.add(new Place("Fabric", "Londres", "Royaume-Uni", "Europe"));
        places.add(new Place("Output", "Brooklyn", "États-Unis", "Amérique du Nord"));
        places.add(new Place("Space", "Ibiza", "Espagne", "Europe"));
        places.add(new Place("Amnesia", "Ibiza", "Espagne", "Europe"));
        places.add(new Place("Pacha", "Ibiza", "Espagne", "Europe"));
        places.add(new Place("Hï Ibiza", "Ibiza", "Espagne", "Europe"));
        places.add(new Place("Cavo Paradiso", "Mykonos", "Grèce", "Europe"));
        places.add(new Place("DC10", "Ibiza", "Espagne", "Europe"));
        places.add(new Place("Watergate", "Berlin", "Allemagne", "Europe"));
        places.add(new Place("Ministry of Sound", "Londres", "Royaume-Uni", "Europe"));
        places.add(new Place("Echostage", "Washington D.C.", "États-Unis", "Amérique du Nord"));
        places.add(new Place("Output", "New York", "États-Unis", "Amérique du Nord"));
        places.add(new Place("Amsterdam Dance Event (ADE)", "Amsterdam", "Pays-Bas", "Europe"));
        places.add(new Place("Womb", "Tokyo", "Japon", "Asie"));
        places.add(new Place("Zouk", "Singapour", "Singapour", "Asie"));
        places.add(new Place("Omnia", "Las Vegas", "États-Unis", "Amérique du Nord"));
        places.add(new Place("Pacha", "Barcelone", "Espagne", "Europe"));
        places.add(new Place("Elrow", "Barcelone", "Espagne", "Europe"));
        places.add(new Place("Rex Club", "Paris", "France", "Europe"));
        places.add(new Place("Concrete", "Paris", "France", "Europe"));
        places.add(new Place("Faust", "Paris", "France", "Europe"));
        places.add(new Place("Warung Beach Club", "Itajaí", "Brésil", "Amérique du Sud"));
        places.add(new Place("D-Edge", "São Paulo", "Brésil", "Amérique du Sud"));
        places.add(new Place("Mandarino Club", "Rio de Janeiro", "Brésil", "Amérique du Sud"));
        places.add(new Place("Crobar Buenos Aires", "Buenos Aires", "Argentine", "Amérique du Sud"));
        places.add(new Place("Cocoricò", "Riccione", "Italie", "Europe"));
        places.add(new Place("Home Nightclub", "Sydney", "Australie", "Océanie"));
        places.add(new Place("Marquee Sydney", "Sydney", "Australie", "Océanie"));
        places.add(new Place("Revolver Upstairs", "Melbourne", "Australie", "Océanie"));
        places.add(new Place("Elsewhere", "Gold Coast", "Australie", "Océanie"));
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("INSERT INTO Lieu (nom_club, ville, pays, continent) VALUES (?, ?, ?, ?)");
            for (Place place : places) {
                statement.setString(1, place.getClubName());
                statement.setString(2, place.getCity());
                statement.setString(3, place.getCountry());
                statement.setString(4, place.getContinent());
                statement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }

    public void DJPopulate() {
        ArrayList<DJ> djs = new ArrayList<>();
        djs.add(new DJ("Lina", "Jonsson", "1980-01-01", "SPFDJ", "Allemagne", Style.HARD_STYLE));
        djs.add(new DJ("Martyna", "Maja", "1980-01-01", "VTSS", "Pologne", Style.HARD_STYLE));
        djs.add(new DJ("Alexandre", "Chennebault", "1980-01-01", "DJ2E", "France", Style.HARD_STYLE));
        djs.add(new DJ("Harris", "Calvin", "1984-01-17", "Calvin Harris", "Royaume-Uni", Style.HOUSE));
        djs.add(new DJ("Garrix", "Martin", "1996-05-14", "Martin Garrix", "Pays-Bas", Style.ELECTRO));
        djs.add(new DJ("Guetta", "David", "1967-11-07", "David Guetta", "France", Style.HARD_STYLE));
        djs.add(new DJ("Tiesto", "Tijs", "1969-01-17", "Tiësto", "Pays-Bas", Style.HOUSE));
        djs.add(new DJ("Avicii", "Tim", "1989-09-08", "Bergling", "Suède", Style.ELECTRO));
        djs.add(new DJ("Prydz", "Eric", "1976-07-19", "Eric Prydz", "Suède", Style.HARD_STYLE));
        djs.add(new DJ("Deadmau5", "Joel", "1981-01-05", "Joel Zimmerman", "Canada", Style.HOUSE));
        djs.add(new DJ("Oakenfold", "Paul", "1963-08-30", "Paul Oakenfold", "Royaume-Uni", Style.ELECTRO));
        djs.add(new DJ("Van Buuren", "Armin", "1976-12-25", "Armin van Buuren", "Pays-Bas", Style.HARD_STYLE));
        djs.add(new DJ("Aoki", "Steve", "1977-11-30", "Steve Aoki", "États-Unis", Style.HOUSE));
        djs.add(new DJ("Diplo", "Wes", "1978-11-10", "Wesley Pentz", "États-Unis", Style.ELECTRO));
        djs.add(new DJ("Hardwell", "Robbert", "1988-01-07", "Hardwell", "Pays-Bas", Style.HARD_STYLE));
        djs.add(new DJ("Calvin", "Harris", "1984-01-17", "Calvin Harris", "Royaume-Uni", Style.HOUSE));
        djs.add(new DJ("Marshmello", "Chris", "1992-05-19", "Chris Comstock", "États-Unis", Style.ELECTRO));
        djs.add(new DJ("Axwell", "Axel", "1977-12-18", "Axel Hedfors", "Suède", Style.HARD_STYLE));
        djs.add(new DJ("Afrojack", "Nick", "1987-09-09", "Nick van de Wall", "Pays-Bas", Style.HOUSE));
        djs.add(new DJ("Skrillex", "Sonny", "1988-01-15", "Sonny Moore", "États-Unis", Style.ELECTRO));
        djs.add(new DJ("Kygo", "Kyrre", "1991-09-11", "Kyrre Gørvell-Dahll", "Norvège", Style.HARD_STYLE));
        djs.add(new DJ("Zedd", "Anton", "1989-09-02", "Anton Zaslavski", "Russie", Style.HOUSE));
        djs.add(new DJ("Angello", "Steve", "1982-11-22", "Steve Angello", "Suède", Style.ELECTRO));
        // Insert the list into the database
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("INSERT INTO DJ (prenom, nom, nom_de_scene, date_naissance, lieu_residence, style_musical) VALUES (?, ?, ?, ?, ?, ?)");
            for (DJ dj : djs) {
                statement.setString(1, dj.getName());
                statement.setString(2, dj.getSurname());
                statement.setString(3, dj.getSceneName());
                statement.setString(4, dj.getBirthDate());
                statement.setString(5, dj.getResidence());
                statement.setInt(6, dj.getStyle().ordinal());
                statement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }

    public void EventPopulate() {
        DJDAO djDAO = new DJDAOImplementation();
        PlaceDAO placeDAO = new PlaceDAOImplementation();
        // Take 10 random DJs and 10 random places
        ArrayList<DJ> djs = djDAO.findByAll();
        ArrayList<Place> places = placeDAO.findByAll();

        ArrayList<Event> events = new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            // Date format: "yyyy-MM-dd HH:00:00"
            // Generate a random date between 2021-01-01 and 2024-12-31
            String startDate =  (2021 + (int)(Math.random() * ((2024 - 2021) + 1))) + "-" + (1 + (int)(Math.random() * ((12 - 1) + 1))) + "-" + (1 + (int)(Math.random() * ((28 - 1) + 1))) + " 22:00:00";
            String endDate =  (2021 + (int)(Math.random() * ((2024 - 2021) + 1))) + "-" + (1 + (int)(Math.random() * ((12 - 1) + 1))) + "-" + (1 + (int)(Math.random() * ((28 - 1) + 1))) + " 06:00:00";
            // Check if the start date is before the end date, if not, swap them
            if(startDate.compareTo(endDate) > 0) {
                String temp = startDate;
                startDate = endDate;
                endDate = temp;
            }
            events.add(new Event(startDate, endDate,  djs.get(i).getId(), places.get(i).getId()));
        }
        // Insert the list into the database
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("INSERT INTO Evenement (date_debut, date_fin, dj, lieu) VALUES (?, ?, ?, ?)");
            for (Event event : events) {
                statement.setString(1, event.getStartDate());
                statement.setString(2, event.getEndDate());
                statement.setInt(3, event.getDjId());
                statement.setInt(4, event.getClubId());
                statement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }

    public void PlaceDelete() {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("DELETE FROM Lieu");
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }

    public void DJDelete() {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("DELETE FROM DJ");
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }

    public void EventDelete() {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("DELETE FROM Evenement");
            statement = connection.prepareStatement("DELETE FROM Historique_evenement");
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }
}
