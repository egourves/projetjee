package fr.enssat.projetjee;

public enum Style {
    ELECTRO, HOUSE, HARD_STYLE, EDM;

    public Style toStyle(String str) {
        switch (str) {
            case "EDM": return Style.EDM;
            case "Electro": return Style.ELECTRO;
            case "House": return Style.HOUSE;
            case "Hard Style": return Style.HARD_STYLE;
            default: return null;
        }
    }

    public Style toStyle(int n) {
        switch (n) {
            case 3: return Style.EDM;
            case 0: return Style.ELECTRO;
            case 1: return Style.HOUSE;
            case 2: return Style.HARD_STYLE;
            default: return null;
        }
    }
}
