package fr.enssat.projetjee;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Path("/djs")
public class DJController {
    //private DJDAO djDAO = new DJDAOMockImpl();
    private DJDAO djDAO = new DJDAOImplementation();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/top5")
    public String getTop5DJs() {
        List<DJ> djs;
        djs= djDAO.top5DJs();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String json = gson.toJson(djs);
        System.out.println("Top5 > "+json);
        return json;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getDJs() {
        List<DJ> djs;
        djs= djDAO.findByAll();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String json = gson.toJson(djs);
        return json;
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public void createDJ(@FormParam("dj_sceneName") String sceneName, @FormParam("dj_name") String name,
                           @FormParam("dj_surname") String surname, @FormParam("dj_residence") String residence,
                         @FormParam("dj_birth") String birth_date, @FormParam("dj_style") String style) {
        djDAO.create(new DJ(name, surname, sceneName, birth_date, residence, Style.EDM.toStyle(style)));
    }

    @PUT
    @Path("/{id}")
    @Consumes("application/x-www-form-urlencoded")
    public void modifyDJ(@PathParam("id") int id,@FormParam("dj_sceneName") String sceneName,
                         @FormParam("dj_name") String name, @FormParam("dj_surname") String surname,
                         @FormParam("dj_residence") String residence, @FormParam("dj_birth") String birth_date,
                         @FormParam("dj_style") String style) {
        //System.out.println("Controller > "+id+";"+sceneName+";"+name+";"+surname+";"+residence+";"+birth_date+";"+style);
        djDAO.update(new DJ(id, name, surname, sceneName, birth_date, residence, Style.EDM.toStyle(style)));
    }

    @DELETE
    @Path("/{id}")
    @Consumes("application/x-www-form-urlencoded")
    public void deleteDJ(@PathParam("id") int id) {
        djDAO.delete(id);
    }
}