package fr.enssat.projetjee;

public class DJ {
    public DJ() {
    }
    public DJ(String name, String surname, String sceneName, String birthDate, String residence, Style style) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.residence = residence;
        this.sceneName = sceneName;
        this.style = style;
    }

    public DJ(int id, String name, String surname, String sceneName, String birthDate, String residence, Style style) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.residence = residence;
        this.sceneName = sceneName;
        this.style = style;
    }

    private int id;
    private String name;
    private String surname;
    private String sceneName;
    private String residence;
    private String birthDate;

    public void setSceneName(String sceneName) {
        this.sceneName = sceneName;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    private Style style; // TO-DO: change to enum

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSceneName() {
        return sceneName;
    }
    public String getBirthDate() {
        return birthDate;
    }
    public String getResidence() {return residence; }

    public Style getStyle() {
        return style;
    }

    public String toString() {
        return "DJ{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", sceneName='" + sceneName + '\'' +
                ", residence='" + residence + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", style=" + style +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this){
            return true;
        } else if (!(obj instanceof DJ)){
            return false;
        } else {
            DJ dj = (DJ) obj;
            return this.id == dj.id && this.name.equals(dj.name) && this.surname.equals(dj.surname) && this.sceneName.equals(dj.sceneName) && this.residence.equals(dj.residence) && this.birthDate.equals(dj.birthDate) && this.style.equals(dj.style);
        }
    }
}


