package fr.enssat.projetjee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Implémentation de l'interface EventDAO pour accéder aux données des événements dans la base de données.
 */
public class EventDAOImplementation implements EventDAO {

    /**
     * Récupère tous les événements depuis la base de données.
     * @return Une liste d'événements.
     */
    @Override
    public ArrayList<Event> findByAll() {
        Connection connection = null;
        Statement statement = null;
        ArrayList<Event> events = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.createStatement();
            var rs = statement.executeQuery("SELECT Evenement.id, date_debut, date_fin, dj, lieu, nom_de_scene, nom_club FROM Evenement JOIN DJ D on D.id = Evenement.dj JOIN info_thanos_schema.Lieu L on L.id = Evenement.lieu;");
            while (rs.next()) {
                events.add(new Event(
                        rs.getInt("id"),
                        rs.getInt("lieu"),
                        rs.getInt("dj"),
                        rs.getString("date_debut"),
                        rs.getString("date_fin"),
                        rs.getString("nom_club"),
                        rs.getString("nom_de_scene")));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return events;
    }

    /**
     * Récupère un événement à partir de son identifiant.
     * @param id L'identifiant de l'événement à récupérer.
     * @return L'événement correspondant à l'identifiant spécifié.
     */
    @Override
    public Event findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        Event event = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, date_debut, date_fin, dj, lieu FROM Evenement WHERE id = ?");
            statement.setInt(1, (id));
            var rs = statement.executeQuery();
            if (rs.next()) {
                event = new Event(
                        rs.getInt("id"),
                        rs.getString("date_debut"),
                        rs.getString("date_fin"),
                        rs.getInt("dj"),
                        rs.getInt("lieu"));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return event;
    }

    @Override
    public ArrayList<Event> findByDate(String searchDate) {
        Connection connection = null;
        PreparedStatement statement = null;
        ArrayList<Event> events = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, date_debut, date_fin, dj, lieu FROM Evenement WHERE date_debut = ? OR date_fin = ?");
            statement.setString(1, searchDate); // java date to sql date conversion
            statement.setString(2, searchDate);
            var rs = statement.executeQuery();
            while (rs.next()) {
                events.add(new Event(
                        rs.getInt("id"),
                        rs.getString("date_debut"),
                        rs.getString("date_fin"),
                        rs.getInt("dj"),
                        rs.getInt("lieu")));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return events;
    }

    /**
     * Crée un nouvel événement dans la base de données.
     * @param event L'événement à créer.
     * @return L'événement créé avec son identifiant.
     */
    @Override
    public Event create(Event event) {
        if (!isEventExist(event)) {
            Connection connection = null;
            PreparedStatement statement = null;
            try {
                connection = DBManager.getInstance().getConnection();
                statement = connection.prepareStatement("INSERT INTO Evenement (date_debut, date_fin, dj, lieu) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, event.getStartDate());
                statement.setString(2, event.getEndDate());
                statement.setInt(3, event.getDjId());
                statement.setInt(4, event.getClubId());
                statement.executeUpdate();
                var rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    event.setId(rs.getInt(1));
                }
            } catch (Exception e) {
                System.out.println("Erreur:" + e);
            } finally {
                // Close connection and statement after execution
                if (connection != null) {
                    try {
                        if (statement != null) {
                            statement.close();
                        }
                        connection.close();
                    } catch (Exception e) {
                        System.out.println("Erreur:" + e);
                    }
                }
            }
        }
        return event;
    }

    @Override
    public Event delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        Event event = findById(id);
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("DELETE FROM Evenement WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return event;
    }

    public boolean isEventExist(Event event) {
        Connection connection = null;
        PreparedStatement statement = null;
        boolean isExist = false;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id FROM Evenement WHERE date_debut = ? AND date_fin = ? AND dj = ? AND lieu = ?");
            statement.setString(1, event.getStartDate());
            statement.setString(2, event.getEndDate());
            statement.setInt(3, event.getDjId());
            statement.setInt(4, event.getClubId());
            var rs = statement.executeQuery();
            if (rs.next()) {
                isExist = true;
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return isExist;
    }
}

