package fr.enssat.projetjee;

import java.util.ArrayList;

public interface EventDAO {
    /**
     * @return ArrayList<Event> : all Events
     */
    ArrayList<Event> findByAll();

    /**
     * @param searchDate: Event date to find
     * @return ArrayList<Event> : empty if not found
     */
    ArrayList<Event> findByDate(String searchDate);

    /**
     * @param id: Event id
     * @return Event : null if not found
     */
    Event findById(int id);

    /**
     * @param event : Event to create
     * @return Event : Event created with id (0 if not created, != 0 if created)
     */
    Event create(Event event);


    /**
     * @param id : Event id
     * @return Event : null if not found
     */
    Event delete(int id);
}
