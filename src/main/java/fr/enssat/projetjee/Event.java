package fr.enssat.projetjee;

public class Event {
    public Event(int id, String startDate, String endDate, int djId, int placeId) {
        this.id = id;
        this.placeId = placeId;
        this.djId = djId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /*
    Constructor used to put an event in the database
     */
    public Event(String startDate, String endDate, int djId, int placeId) {
        this.placeId = placeId;
        this.djId = djId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /*
    Constructor used to get an event from the database for the controller
     */
    public Event(int id, int placeId, int djId, String startDate, String endDate, String clubName, String djSceneName) {
        this.id = id;
        this.placeId = placeId;
        this.djId = djId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.clubName = clubName;
        this.djSceneName = djSceneName;
    }

    private int id;
    private int placeId;
    private int djId;
    private String startDate;
    private String endDate;

    private String clubName;
    private String djSceneName;

    //getters and setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getClubId() {
        return placeId;
    }

    public int getDjId() {
        return djId;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    //Methode equals() pour la comparaison d'objets
    @Override
    public boolean equals(Object obj) {
        if(obj == this){
            return true;
        } else if (!(obj instanceof Event)){
            return false;
        } else {
            Event event = (Event) obj;
            return this.id == event.id && this.placeId == event.placeId && this.djId == event.djId && this.startDate.equals(event.startDate) && this.endDate.equals(event.endDate);
        }
    }

    public void setDjId(int djId) {
        this.djId = djId;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", placeId=" + placeId +
                ", djId=" + djId +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
