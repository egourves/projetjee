package fr.enssat.projetjee;

public class Place {
    /**
     * Constructeur avec les détails de base du lieu.
     * @param clubName Le nom du club ou du lieu.
     * @param city La ville où se trouve le lieu.
     * @param country Le pays où se trouve le lieu.
     * @param continent Le continent où se trouve le lieu.
     */
    public Place(String clubName, String ville, String pays, String continent) {
        this.clubName = clubName;
        this.city = ville;
        this.country = pays;
        this.continent = continent;
    }

    /**
     * Constructeur avec ID et détails du lieu.
     * @param id L'ID unique du lieu.
     * @param clubName Le nom du club ou du lieu.
     * @param city La ville où se trouve le lieu.
     * @param country Le pays où se trouve le lieu.
     * @param continent Le continent où se trouve le lieu.
     */
    public Place(int id, String clubName, String ville, String pays, String continent) {
        this.id = id;
        this.clubName = clubName;
        this.city = ville;
        this.country = pays;
        this.continent = continent;
    }

    private int id;
    private String clubName;
    private String city;
    private String country;
    private String continent;

    //getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }
    // / Méthodes equals() pour la comparaison d'objets
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (!(obj instanceof Place)) {
            return false;
        } else {
            Place place = (Place) obj;
            return this.id == place.id && this.clubName.equals(place.clubName) && this.city.equals(place.city) && this.country.equals(place.country) && this.continent.equals(place.continent);
        }
    }
}
