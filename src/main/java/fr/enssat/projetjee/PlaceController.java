package fr.enssat.projetjee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

/**
 * Contrôleur REST pour les lieux.
 */
@Path("places")
public class PlaceController {

    private PlaceDAO dao = new PlaceDAOImplementation();

    /**
     * Endpoint REST pour récupérer la liste des lieux au format JSON.
     * @return La liste des lieux au format JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getLieux() {
        List<Place> lieux = null;
        lieux = dao.findByAll();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String json = gson.toJson(lieux);
        return json;
    }


}
