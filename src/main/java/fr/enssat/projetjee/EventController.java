package fr.enssat.projetjee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

/**
 * Contrôleur REST pour gérer les événements.
 */
@Path("events")
public class EventController {
    private final EventDAO eventDAO = new EventDAOImplementation();

    /**
     * Récupère tous les événements.
     * @return Une chaîne JSON représentant tous les événements.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getEvents() {
        List<Event> events;
        events = eventDAO.findByAll();


        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(events);
    }

    /**
     * Récupère les statistiques sur les événements.
     * @return Une chaîne JSON représentant les statistiques sur les événements.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/stats")
    public String getEventStats() {
        List<Event> events;
        int nbEvents;
        int nbDJ = 0;
        List<Integer> listDJID = new ArrayList<>(); // Initialisez listDJID avec une nouvelle ArrayList
        events = eventDAO.findByAll();
        nbEvents = events.size();
        for (Event event : events) {
            if (!listDJID.contains(event.getDjId())) {
                nbDJ++;
                listDJID.add(event.getDjId());
            }
        }
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        // Crée un objet JSON avec les clés et les valeurs appropriées
        JsonObject statsObject = new JsonObject();
        statsObject.addProperty("djCount", nbDJ);
        statsObject.addProperty("eventCount", nbEvents);

        // Convertit l'objet JSON en une chaîne JSON
        return gson.toJson(statsObject);
    }


    /**
     * La procédure createEvent permet de crée un événement lorsque l'on envoie une requête POST.
     * @param placeId L'identifiant du club
     * @param djId L'identifiant du DJ
     * @param startDate La date de début de l'événement
     * @param endDate La date de fin de l'événement
     */
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public void createEvent(@FormParam("placeId") int placeId, @FormParam("djId") int djId, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate) {
        Event event = new Event(startDate, endDate, djId, placeId);
        eventDAO.create(event);
    }

    /**
     * Supprime un événement à partir de son identifiant.
     * @param id L'identifiant de l'événement à supprimer.
     */
    @DELETE
    @Path("/{id}")
    @Consumes("application/x-www-form-urlencoded")
    public void deleteEvent(@PathParam("id") int id) {
        eventDAO.delete(id);
    }
}

