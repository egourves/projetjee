package fr.enssat.projetjee;

import java.util.ArrayList;

public interface DJDAO {
    /**
     * @return all DJs
     */
    ArrayList<DJ> findByAll();

    /**
     * @param id: DJ id to find
     * @return DJ, null if not found
     */
    DJ findById(int id);

    /**
     * @param name: DJ name
     * @return ArrayList<DJ>, empty if not found
     */
    ArrayList<DJ> findByName(String name);

    /**
     * @param dj: DJ to create
     * @return DJ: DJ created with id (0 if not created, != 0 if created)
     */
    DJ create(DJ dj);

    /**
     * @param dj: DJ to update
     */
    void update(DJ dj);

    /**
     * @param id: DJ id
     * @return DJ, null if not found
     */
    DJ delete(int id);

    /**
     * @return top 5 DJs
     */
    ArrayList<DJ> top5DJs();
}
