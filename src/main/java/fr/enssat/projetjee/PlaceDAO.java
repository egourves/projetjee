package fr.enssat.projetjee;

import java.util.ArrayList;

public interface PlaceDAO {
    /**
     * @return ARrayList<Place> : all Places
     */
    ArrayList<Place> findByAll();

    /**
     * @param id: Place id to find
     * @return Place : null if not found
     */
    Place findById(int id);

    /**
     * @param clubName: Place clubName
     * @return ArrayList<Place> : empty if not found
     */
    ArrayList<Place> findByClubName(String clubName);

    /**
     * @param country: Place location by country
     * @return ArrayList<Place>, empty if not found
     */
    ArrayList<Place> findByCountry(String country);

    /**
     * @param city: Place location by city
     * @return ArrayList<Place>, empty if not found
     */
    ArrayList<Place> findByCity(String city);

    /**
     * @param place: Place to create
     * @return Place: Place created with id (0 if not created, != 0 if created)
     */
    Place create(Place place);

    /**
     * @param place: Place to update
     */
    void update(Place place);

    /**
     * @param id: Place id
     * @return Place : null if not found
     */
    Place delete(int id);
}
