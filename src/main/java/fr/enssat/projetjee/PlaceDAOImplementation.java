package fr.enssat.projetjee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Implémentation de l'interface PlaceDAO pour l'accès aux données des lieux depuis une base de données.
 */
public class PlaceDAOImplementation implements PlaceDAO {
    /**
     * Constructeur par défaut.
     */
    public PlaceDAOImplementation() {
    }

    /**
     * Récupère tous les lieux depuis la base de données.
     * @return Une liste de lieux.
     */
    @Override
    public ArrayList<Place> findByAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ArrayList<Place> places = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, nom_club, ville, pays, continent FROM Lieu;");
            var rs = statement.executeQuery();
            while (rs.next()) {
                places.add(new Place(
                        rs.getInt("id"),
                        rs.getString("nom_club"),
                        rs.getString("ville"),
                        rs.getString("pays"),
                        rs.getString("continent")));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return places;
    }

    /**
     * Récupère un lieu à partir de son identifiant.
     * @param id L'identifiant du lieu.
     * @return Le lieu correspondant à l'identifiant, ou null s'il n'existe pas.
     */
    @Override
    public Place findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        Place place = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, nom_club, ville, pays, continent FROM Lieu WHERE id = ?");
            statement.setInt(1, id);
            var rs = statement.executeQuery();
            while (rs.next()) {
                place = new Place(
                        rs.getInt("id"),
                        rs.getString("nom_club"),
                        rs.getString("ville"),
                        rs.getString("pays"),
                        rs.getString("continent"));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return place;
    }

    /**
     * Récupère tous les lieux ayant un nom de club donné.
     * @param clubName Le nom du club.
     * @return Une liste de lieux ayant le nom de club spécifié.
     */
    @Override
    public ArrayList<Place> findByClubName(String clubName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ArrayList<Place> places = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, nom_club, ville, pays, continent FROM Lieu WHERE nom_club = ?");
            statement.setString(1, clubName);
            var rs = statement.executeQuery();
            while (rs.next()) {
                places.add(new Place(
                        rs.getInt("id"),
                        rs.getString("nom_club"),
                        rs.getString("ville"),
                        rs.getString("pays"),
                        rs.getString("continent")));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return places;
    }

    /**
     * Récupère tous les lieux situés dans un pays donné.
     * @param country Le nom du pays.
     * @return Une liste de lieux situés dans le pays spécifié.
     */
    @Override
    public ArrayList<Place> findByCountry(String country) {
        Connection connection = null;
        PreparedStatement statement = null;
        ArrayList<Place> places = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, nom_club, ville, pays, continent FROM Lieu WHERE pays = ?");
            statement.setString(1, country);
            var rs = statement.executeQuery();
            while (rs.next()) {
                places.add(new Place(
                        rs.getInt("id"),
                        rs.getString("nom_club"),
                        rs.getString("ville"),
                        rs.getString("pays"),
                        rs.getString("continent")));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return places;
    }

    /**
     * Récupère tous les lieux situés dans une ville donnée.
     * @param city Le nom de la ville.
     * @return Une liste de lieux situés dans la ville spécifiée.
     */
    @Override
    public ArrayList<Place> findByCity(String city) {
        Connection connection = null;
        PreparedStatement statement = null;
        ArrayList<Place> places = new ArrayList<>();
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id, nom_club, ville, pays, continent FROM Lieu WHERE ville = ?");
            statement.setString(1, city);
            var rs = statement.executeQuery();
            while (rs.next()) {
                places.add(new Place(
                        rs.getInt("id"),
                        rs.getString("nom_club"),
                        rs.getString("ville"),
                        rs.getString("pays"),
                        rs.getString("continent")));
            }
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return places;
    }

    /**
     * Crée un nouveau lieu dans la base de données.
     * @param place Le lieu à créer.
     * @return Le lieu créé avec son identifiant.
     */
    @Override
    public Place create(Place place) {
        if (!isPlaceExist(place)) {
            Connection connection = null;
            PreparedStatement statement = null;
            try {
                connection = DBManager.getInstance().getConnection();
                statement = connection.prepareStatement("INSERT INTO Lieu(nom_club, ville, pays, continent) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, place.getClubName());
                statement.setString(2, place.getCity());
                statement.setString(3, place.getCountry());
                statement.setString(4, place.getContinent());
                statement.executeUpdate();
                var rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    place.setId(rs.getInt(1));
                }
            } catch (Exception e) {
                System.out.println("Erreur:" + e);
            } finally {
                // Close connection and statement after execution
                if (connection != null) {
                    try {
                        if (statement != null) {
                            statement.close();
                        }
                        connection.close();
                    } catch (Exception e) {
                        System.out.println("Erreur:" + e);
                    }
                }
            }
        }
        return place;
    }

    /**
     * Met à jour un lieu dans la base de données.
     * @param place Le lieu à mettre à jour.
     */
    @Override
    public void update(Place place) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("UPDATE Lieu SET nom_club = ?, ville = ?, pays = ?, continent = ? WHERE id = ?");
            statement.setString(1, place.getClubName());
            statement.setString(2, place.getCity());
            statement.setString(3, place.getCountry());
            statement.setString(4, place.getContinent());
            statement.setInt(5, place.getId());
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
    }

    /**
     * Supprime un lieu de la base de données à partir de son identifiant.
     * @param id L'identifiant du lieu à supprimer.
     * @return Le lieu supprimé.
     */

    public Place delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        Place place = null;
        try {
            connection = DBManager.getInstance().getConnection();
            place = findById(id);
            statement = connection.prepareStatement("DELETE FROM Lieu WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return place;
    }

    public boolean isPlaceExist(Place place) {
        Connection connection = null;
        PreparedStatement statement = null;
        boolean isExist = false;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT id FROM Lieu WHERE nom_club = ? AND ville = ? AND pays = ? AND continent = ?");
            statement.setString(1, place.getClubName());
            statement.setString(2, place.getCity());
            statement.setString(3, place.getCountry());
            statement.setString(4, place.getContinent());
            var rs = statement.executeQuery();
            isExist = rs.next();
        } catch (Exception e) {
            System.out.println("Erreur:" + e);
        } finally {
            // Close connection and statement after execution
            if (connection != null) {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connection.close();
                } catch (Exception e) {
                    System.out.println("Erreur:" + e);
                }
            }
        }
        return isExist;
    }
}
