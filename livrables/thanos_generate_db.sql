-- MySQL Workbench Forward Engineering
-- @author équipe Thanos 2023-2024

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema info_thanos_schema
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema info_thanos_schema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `info_thanos_schema` ;
USE `info_thanos_schema` ;

-- -----------------------------------------------------
-- Table `info_thanos_schema`.`DJ`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `info_thanos_schema`.`DJ` ;

CREATE TABLE IF NOT EXISTS `info_thanos_schema`.`DJ` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `prenom` VARCHAR(45) NOT NULL,
  `nom_de_scene` VARCHAR(45) NULL,
  `date_naissance` DATE NOT NULL,
  `lieu_residence` VARCHAR(45) NULL,
  `style_musical` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `info_thanos_schema`.`Lieu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `info_thanos_schema`.`Lieu` ;

CREATE TABLE IF NOT EXISTS `info_thanos_schema`.`Lieu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom_club` VARCHAR(45) NOT NULL,
  `ville` VARCHAR(45) NOT NULL,
  `pays` VARCHAR(45) NOT NULL,
  `continent` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `info_thanos_schema`.`Evenement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `info_thanos_schema`.`Evenement` ;

CREATE TABLE IF NOT EXISTS `info_thanos_schema`.`Evenement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_debut` TIMESTAMP NOT NULL,
  `date_fin` TIMESTAMP NOT NULL,
  `dj` INT NOT NULL,
  `lieu` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `evenement_dj_fkey_idx` (`dj` ASC) VISIBLE,
  INDEX `evenement_lieu_fkey_idx` (`lieu` ASC) VISIBLE,
  CONSTRAINT `evenement_dj_fkey`
    FOREIGN KEY (`dj`)
    REFERENCES `info_thanos_schema`.`DJ` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `evenement_lieu_fkey`
    FOREIGN KEY (`lieu`)
    REFERENCES `info_thanos_schema`.`Lieu` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `info_thanos_schema`.`Historique_evenement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `info_thanos_schema`.`Historique_evenement` ;

CREATE TABLE IF NOT EXISTS `info_thanos_schema`.`Historique_evenement` (
  `id` INT NOT NULL,
  `date_debut` TIMESTAMP NOT NULL,
  `date_fin` TIMESTAMP NOT NULL,
  `dj` INT NOT NULL,
  `lieu` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `historique_evenement_lieu_fkey_idx` (`lieu` ASC) VISIBLE,
  INDEX `historique_evenement_dj_fkey_idx` (`dj` ASC) VISIBLE,
  CONSTRAINT `historique_evenement_dj_fkey`
    FOREIGN KEY (`dj`)
    REFERENCES `info_thanos_schema`.`DJ` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `historique_evenement_lieu_fkey`
    FOREIGN KEY (`lieu`)
    REFERENCES `info_thanos_schema`.`Lieu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `info_thanos_schema`;

DELIMITER $$

USE `info_thanos_schema`$$
DROP TRIGGER IF EXISTS `info_thanos_schema`.`Prevent_simultaneous_events_BEFORE_INSERT` $$
USE `info_thanos_schema`$$
CREATE DEFINER = CURRENT_USER TRIGGER `info_thanos_schema`.`Prevent_simultaneous_events_BEFORE_INSERT` BEFORE INSERT ON `Evenement` FOR EACH ROW
BEGIN
	DECLARE count_events INT;
    SELECT COUNT(*) INTO count_events
    FROM Evenement
    WHERE lieu = NEW.lieu
    AND (date_debut BETWEEN NEW.date_debut AND NEW.date_fin
        OR date_fin BETWEEN NEW.date_debut AND NEW.date_fin);
    IF count_events > 0 THEN
        SIGNAL SQLSTATE '45001'
        SET MESSAGE_TEXT = 'Impossible d\'organiser deux événements simultanés au même endroit avec deux DJs différents.';
    END IF;
END$$


USE `info_thanos_schema`$$
DROP TRIGGER IF EXISTS `info_thanos_schema`.`Require_24h_period_same_continent_BEFORE_INSERT` $$
USE `info_thanos_schema`$$
CREATE DEFINER = CURRENT_USER TRIGGER `info_thanos_schema`.`Require_24h_period_same_continent_BEFORE_INSERT` BEFORE INSERT ON `Evenement` FOR EACH ROW
BEGIN
	DECLARE last_event_before TIMESTAMP; -- Date de fin du dernier événement ayant lieu avant celui que l'on désire ajouter
    DECLARE first_event_after TIMESTAMP; -- Date de début du premier événement ayant lieu après celui que l'on désire ajouter
    DECLARE continent_last_event_before VARCHAR(45); -- Continent sur lequel a lieu 'last_event_before'
    DECLARE continent_first_event_after VARCHAR(45); -- Continent sur lequel a lieu 'first_event_after'
    
    -- Recherche de l'événement de date de fin la plus proche par rapport à la date de début de l'événement que l'on veut insérer
    SELECT MAX(date_fin) INTO last_event_before
    FROM Evenement
    WHERE dj = NEW.dj AND date_fin < NEW.date_debut;
    
    -- Recherche de l'événement de date de début la plus proche par rapport à la date de fin de l'événement que l'on veut insérer
    SELECT MIN(date_debut) INTO first_event_after
    FROM Evenement
    WHERE dj = NEW.dj AND date_debut < NEW.date_fin;
    
    -- Vérification de la contrainte pour l'événement juste avant celui à insérer
    IF last_event_before IS NOT NULL THEN
		SELECT continent INTO continent_last_event_before
        FROM Lieu
        WHERE id = (SELECT lieu FROM Evenement WHERE date_fin = last_event_before);
        
        IF continent_last_event_before = (SELECT continent from Lieu WHERE id = NEW.lieu) THEN
			IF ABS(TIMESTAMPDIFF(HOUR, last_event_before, NEW.date_debut)) < 24 THEN
				SIGNAL SQLSTATE '45002'
				SET MESSAGE_TEXT = 'Il faut respecter une période de 24h minimum entre deux événements sur le même continent pour un même DJ.';
			END IF;
        END IF;
	END IF;
    
    -- Vérification de la contrainte pour l'événement juste après celui à insérer
    IF first_event_after IS NOT NULL THEN
		SELECT continent INTO continent_first_event_after
        FROM Lieu
        WHERE id = (SELECT lieu FROM Evenement WHERE date_debut = first_event_after);
        
        IF continent_first_event_after = (SELECT continent from Lieu WHERE id = NEW.lieu) THEN
			IF ABS(TIMESTAMPDIFF(HOUR, NEW.date_fin, first_event_after)) < 24 THEN
				SIGNAL SQLSTATE '45002'
                SET MESSAGE_TEXT = 'Il faut respecter une période de 24h minimum entre deux événements sur le même continent pour un même DJ.';
			END IF;
		END IF;
	END IF;
END$$


USE `info_thanos_schema`$$
DROP TRIGGER IF EXISTS `info_thanos_schema`.`Require_48h_period_different_continents_BEFORE_INSERT` $$
USE `info_thanos_schema`$$
CREATE DEFINER = CURRENT_USER TRIGGER `info_thanos_schema`.`Require_48h_period_different_continents_BEFORE_INSERT` BEFORE INSERT ON `Evenement` FOR EACH ROW
BEGIN
	DECLARE last_event_before TIMESTAMP; -- Date de fin du dernier événement ayant lieu avant celui que l'on désire ajouter
    DECLARE first_event_after TIMESTAMP; -- Date de début du premier événement ayant lieu après celui que l'on désire ajouter
    
    -- Recherche de l'événement de date de fin la plus proche par rapport à la date de début de l'événement que l'on veut insérer
    SELECT MAX(date_fin) INTO last_event_before
    FROM Evenement
    WHERE dj = NEW.dj AND date_fin < NEW.date_debut;
    
    -- Recherche de l'événement de date de début la plus proche par rapport à la date de fin de l'événement que l'on veut insérer
    SELECT MIN(date_debut) INTO first_event_after
    FROM Evenement
    WHERE dj = NEW.dj AND date_debut < NEW.date_fin;
    
    IF last_event_before IS NOT NULL THEN
        IF ABS(TIMESTAMPDIFF(HOUR, last_event_before, NEW.date_debut)) < 48 THEN
            SIGNAL SQLSTATE '45003'
            SET MESSAGE_TEXT = 'Il faut respecter une période de 48h minimum entre deux événements sur des continents différents pour un même DJ.';
        END IF;
    END IF;
    
     IF first_event_after IS NOT NULL THEN
        IF ABS(TIMESTAMPDIFF(HOUR, NEW.date_fin, first_event_after)) < 48 THEN
            SIGNAL SQLSTATE '45003'
            SET MESSAGE_TEXT = 'Il faut respecter une période de 48h minimum entre deux événements sur des continents différents pour un même DJ.';
        END IF;
    END IF;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `info_thanos_schema`.`DJ`
-- -----------------------------------------------------
START TRANSACTION;
USE `info_thanos_schema`;
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (1, 'Lina', 'Jonsson', 'SPFDJ', '1980-01-01', 'Allemagne', 0);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (2, 'Martyna', 'Maja', 'VTSS', '1980-01-01', 'Pologne', 0);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (3, 'Alexandre', 'Chennebault', 'DJ2E', '1980-01-01', 'France', 0);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (4, 'Harris', 'Calvin', 'Calvin Harris', '1984-01-17', 'Royaume-Uni', 2);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (5, 'Garrix', 'Martin', 'Martin Garrix', '1996-05-14', 'Pays-Bas', 1);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (6, 'Guetta', 'David', 'David Guetta', '1967-11-07', 'France', 0);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (7, 'Tiesto', 'Tijs', 'Tiësto', '1969-01-17', 'Pays-Bas', 3);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (8, 'Avicii', 'Tim', 'Bergling', '1989-09-08', 'Suède', 1);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (9, 'Prydz', 'Eric', 'Eric Prydz', '1976-07-19', 'Suède', 2);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (10, 'Deadmau5', 'Joel', 'Joel Zimmerman', '1981-01-05', 'Canada', 3);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (11, 'Oakenfold', 'Paul', 'Paul Oakenfold', '1963-08-30', 'Royaume-Uni', 0);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (12, 'Van Buuren', 'Armin', 'Armin van Buuren', '1976-12-25', 'Pays-Bas', 3);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (13, 'Aoki', 'Steve', 'Steve Aoki', '1977-11-30', 'États-Unis', 2);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (14, 'Diplo', 'Wes', 'Wesley Pentz', '1978-11-10', 'États-Unis', 1);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (15, 'Hardwell', 'Robbert', 'Hardwell', '1988-01-07', 'Pays-Bas', 3);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (16, 'Calvin', 'Harris', 'Calvin Harris', '1984-01-17', 'Royaume-Uni', 2);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (17, 'Marshmello', 'Chris', 'Chris Comstock', '1992-05-19', 'États-Unis', 1);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (18, 'Axwell', 'Axel', 'Axel Hedfors', '1977-12-18', 'Suède', 2);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (19, 'Afrojack', 'Nick', 'Nick van de Wall', '1987-09-09', 'Pays-Bas', 2);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (20, 'Skrillex', 'Sonny', 'Sonny Moore', '1988-01-15', 'États-Unis', 3);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (21, 'Kygo', 'Kyrre', 'Kyrre Gørvell-Dahll', '1991-09-11', 'Norvège', 1);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (22, 'Zedd', 'Anton', 'Anton Zaslavski', '1989-09-02', 'Russie', 1);
INSERT INTO `info_thanos_schema`.`DJ` (`id`, `nom`, `prenom`, `nom_de_scene`, `date_naissance`, `lieu_residence`, `style_musical`) VALUES (23, 'Angello', 'Steve', 'Steve Angello', '1982-11-22', 'Suède', 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `info_thanos_schema`.`Lieu`
-- -----------------------------------------------------
START TRANSACTION;
USE `info_thanos_schema`;
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (1, 'Berghain', 'Berlin', 'Allemagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (2, 'Fabric', 'Londres', 'Royaume-Uni', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (3, 'Output', 'Brooklyn', 'États-Unis', 'Amérique du Nord');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (4, 'Space', 'Ibiza', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (5, 'Amnesia', 'Ibiza', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (6, 'Pacha', 'Ibiza', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (7, 'Ushuaïa', 'Ibiza', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (8, 'Hï Ibiza', 'Ibiza', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (9, 'Cavo Paradiso', 'Mykonos', 'Grèce', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (10, 'DC10', 'Ibiza', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (11, 'Watergate', 'Berlin', 'Allemagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (12, 'Ministry of Sound', 'Londres', 'Royaume-Uni', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (13, 'Echostage', 'Washington D.C.', 'États-Unis', 'Amérique du Nord');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (14, 'Output', 'New York', 'États-Unis', 'Amérique du Nord');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (15, 'Amsterdam Dance Event (ADE)', 'Amsterdam', 'Pays-Bas', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (16, 'Womb', 'Tokyo', 'Japon', 'Asie');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (17, 'Zouk', 'Singapour', 'Singapour', 'Asie');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (18, 'Omnia', 'Las Vegas', 'États-Unis', 'Amérique du Nord');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (19, 'Pacha', 'Barcelone', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (20, 'Elrow', 'Barcelone', 'Espagne', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (21, 'Rex Club', 'Paris', 'France', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (22, 'Concrete', 'Paris', 'France', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (23, 'Faust', 'Paris', 'France', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (24, 'Warung Beach Club', 'Itajaí', 'Brésil', 'Amérique du Sud');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (25, 'D-Edge', 'São Paulo', 'Brésil', 'Amérique du Sud');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (26, 'Mandarino Club', 'Rio de Janeiro', 'Brésil', 'Amérique du Sud');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (27, 'Crobar Buenos Aires', 'Buenos Aires', 'Argentine', 'Amérique du Sud');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (28, 'Cocoricò', 'Riccione', 'Italie', 'Europe');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (29, 'Home Nightclub', 'Sydney', 'Australie', 'Océanie');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (30, 'Marquee Sydney', 'Sydney', 'Australie', 'Océanie');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (31, 'Revolver Upstairs', 'Melbourne', 'Australie', 'Océanie');
INSERT INTO `info_thanos_schema`.`Lieu` (`id`, `nom_club`, `ville`, `pays`, `continent`) VALUES (32, 'Elsewhere', 'Gold Coast', 'Australie', 'Océanie');

COMMIT;

-- begin attached script 'event_archive_evenement'
delimiter $$

DROP EVENT IF EXISTS event_archive_evenement$$

CREATE EVENT IF NOT EXISTS event_archive_evenement ON SCHEDULE EVERY 1 MINUTE DO
BEGIN
	SET @current_time = NOW();
	INSERT INTO Historique_evenement(id, date_debut,date_fin,dj,lieu)
	SELECT id, date_debut, date_fin, dj, lieu
	FROM Evenement
	WHERE TIMESTAMPDIFF(MINUTE, date_fin, @current_time) > 0;
            
	DELETE FROM Evenement
	WHERE TIMESTAMPDIFF(MINUTE, date_fin, @current_time) > 0;
END$$

delimiter ;
-- end attached script 'event_archive_evenement'
-- begin attached script 'event_purge_historique'
delimiter $$

DROP EVENT IF EXISTS event_purge_historique$$

CREATE EVENT IF NOT EXISTS event_purge_historique ON SCHEDULE EVERY 1 DAY DO
BEGIN
	SET @current_time = NOW();

	DELETE FROM Historique_evenement
	WHERE TIMESTAMPDIFF(DAY,date_fin,@current_time) > 365;
END$$

delimiter ;

-- end attached script 'event_purge_historique'
