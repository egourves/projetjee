### Installation de DataTables dans une page HTML

Dans ce tutoriel, nous allons apprendre à installer et à utiliser DataTables pour afficher des données tabulaires dans une page HTML.

#### Étapes à suivre :

1. **Inclure jQuery :**
   Avant d'inclure DataTables, assure-toi d'inclure la bibliothèque jQuery dans ta page. DataTables dépend de jQuery pour fonctionner correctement.

   ```html
   <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
   ```

2. **Inclure DataTables :**
   Ensuite, incluons DataTables en ajoutant le script JavaScript et la feuille de style CSS nécessaires.

   ```html
   <script src="https://cdn.datatables.net/1.11.6/js/jquery.dataTables.min.js"></script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.11.6/css/jquery.dataTables.min.css">
   ```

3. **Structure de base de la table :**
   Crée une structure HTML de base pour la table dans laquelle tu souhaites afficher les données.

   ```html
   <table id="eventtable" class="display">
       <thead>
           <tr>
               <th>Disc Jockey</th>
               <th>Date</th>
               <th>Nom du club</th>
               <th>Heure début</th>
               <th>Heure fin</th>
           </tr>
       </thead>
       <tbody>
           <!-- Les données seront ajoutées dynamiquement ici -->
       </tbody>
   </table>
   ```

4. **Chargement des données :**
   Utilise JavaScript pour charger les données dans la table à partir de ta source de données.

   ```html
   <script>
       function loadEvents(url) {
           fetch(url)
               .then(response => response.json())
               .then(response => process(response))
               .catch(error => alert("Err : " + error));
       }

       function process(events) {
           var table = $('#eventtable').DataTable(); // Initialise DataTables sur ton tableau

           table.clear(); // Efface les données existantes dans le tableau

           events.forEach(event => {
               table.row.add([ // Ajoute une nouvelle ligne de données au tableau
                   event.DJ,
                   event.date,
                   event.club_name,
                   event.heure_debut,
                   event.heure_fin
               ]).draw(); // Redessine le tableau avec les nouvelles données
           });
       }
   </script>
   ```

5. **Appel de la fonction de chargement :**
   Enfin, appelle la fonction `loadEvents()` avec l'URL de ta source de données pour charger les événements dans la table lors du chargement de la page.

   ```html
   <body onload="loadEvents('/projetjee_war_exploded/api/event-management/events')">
   ```

#### Conclusion :

En suivant ces étapes, tu pourras installer et utiliser DataTables pour afficher des données tabulaires dans une page HTML. N'oublie pas d'ajuster l'URL de chargement des données en fonction de ta propre configuration.